angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ngCordova'])

.run(function($ionicPlatform,$rootScope,$location,$http,$state,User,$ionicActionSheet,$timeout, $urlRouter,$templateCache) {
  $rootScope.$on('$viewContentLoaded', function(event) {
      $templateCache.removeAll();
   });
  Highcharts.setOptions({global:{useUTC:false}});
  $rootScope.token = null;
  $rootScope.setToken = function(token){
    $http.defaults.headers.common['token'] = token;
  }
  $rootScope.hostName = "http://api.ubisen.com";
  $rootScope.showError = function(input) {
    var hideSheet = $ionicActionSheet.show({
     titleText: input,
     // cancelText: 'OK',
     cancel: function() {
        },
     buttonClicked: function(index) {
       return true;
     }
   });
   $timeout(function() {
     hideSheet();
   }, 1000);
  };
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$httpProvider) {
  delete $httpProvider.defaults.headers.common["X-Requested-With"];
  $stateProvider
    .state('Main', {
        url: "/",
        abstract:true,
        controller: 'MainCtrl'
    })
    .state('login', {
      url: "/login",
      views: {
        'content': {
          templateUrl: 'templates/login.html',
          controller: 'AllCtrl'
        }
      }
    })
    .state('account', {
      url: "/account",
      views: {
        'content': {
          templateUrl: 'templates/account.html',
          controller: 'AllCtrl'
        }
      }
    })
    .state('regions', {
      url: "/regions",
      views: {
        'content': {
          templateUrl: 'templates/regions.html',
          controller: 'AllCtrl'
        }
      }
    })
    .state('newRegion', {
      url: "/regions/newRegion",
      views: {
        'content': {
          templateUrl: 'templates/newregion.html',
          controller: 'AllCtrl'
        }
      }
    })
    .state('region', {
      url: "/regions/{regionId}",
      views: {
        'content': {
          templateUrl: 'templates/garden.html',
          controller: 'AllCtrl'
        }
      }
    })
    .state('newNode', {
      url: "/regions/{regionId}/newNode",
      views: {
        'content': {
          templateUrl: 'templates/scancode.html',
          controller: 'AllCtrl'
        }
      }
    })
    .state('bridge', {
      url: "/regions/{regionId}/bridge/{id}",
      views: {
        'content': {
          templateUrl: 'templates/bridge.html',
          controller: 'AllCtrl'
        }
      }
    })
    ;
  $urlRouterProvider.otherwise('/login');
});

