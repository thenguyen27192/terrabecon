// var socket;
angular.module('starter.controllers', ['ionic','ui.router','ngCordova','highcharts-ng','rzModule'])//'ui.slider',
//,'uiSlider'
.controller('MainCtrl', function($scope, $stateParams,$rootScope ,$state,$http,User) {
  $scope.gListRegion= [];
  $scope.gCurrentRegion = {};
  $scope.gListDevice= [];
  $scope.gCurrentDevice = {};
  $scope.gListSensor= [];

  $scope.socket =0;
  $scope.addMessage = function(id, title, message){
    ionic.Platform.ready(function(){
      window.plugin.notification.local.add({id: id,title: title,message: message,date: new Date(new Date().getTime() + 60*1000)});
    });
  }

})
.controller('AllCtrl',  function($ionicLoading,$cordovaToast,$scope,$http, $log,$stateParams, $location, $rootScope,$state,User,Region,$cordovaBarcodeScanner,Device,$window,$timeout,$ionicSlideBoxDelegate,$interval,$templateCache) {
  //state = login
  var socket = null;
  $scope.loading = function(){$ionicLoading.show({template: 'Loading...'});}
  $scope.hiding = function(){$ionicLoading.hide();};
  if($rootScope.token!=null)
  socket = io.connect("http://api.ubisen.com:80?token="+$rootScope.token);
  if($state.$current.self.name == "login"){
   
    $scope.tabsLogin = 1;
    $scope.userlogin ={email:'',password:''};
    $scope.userSignUp = {};
    $scope.$parent.addMessage(1,'SmartGrow','SmartGrow running.');
    $scope.submitUser = function () {
      if($scope.tabsLogin == 1){
       User.login($scope.userlogin,function (err,data) {
         if(err){
            $cordovaToast.showLongBottom(err.errors).then(function(success) {
            }, function (error) {
            });
          }
          else if(data){
              $http.defaults.headers.common['token'] = data.token;
              $rootScope.token = data.token;
              window.localStorage['token'] = data.token;
              socket = io.connect("http://api.ubisen.com:80?token="+$rootScope.token);
              $scope.$parent.addMessage(Math.floor((Math.random()*6)+1),'SmartGrow','logined account:'+ $scope.userlogin.email);
              $scope.userlogin.password ='';
              window.location = '#/regions';
            }
       })
      }
      else{
        User.create($scope.userSignUp,function (err,data) {
          if(err){
            $cordovaToast.showLongBottom(err.errors).then(function(success) {}, function (error) {});
          }
          {
            if(data){
              $scope.tabs = 1;
              $scope.userlogin.email = $scope.userSignUp.email;
              $scope.userSignUp ={email:'',password:'',repassword:''};
              // $rootScope.showError("Sign Up complete!");
              $cordovaToast.showLongBottom("Sign Up complete!").then(function(success) {}, function (error) {});
              $scope.tabsLogin = 1;
            }
          }
         });
      }
    };
    $scope.activeForm = function (e){
      if(e.keyCode == 13)
        $scope.submitUser();
    }
  }

  //accountCtrl
  if($state.$current.self.name == "account" ){
    if($rootScope.token == null){
      var token  = window.localStorage['token'];
      if(typeof token != "undefined" && token != ""){
        $rootScope.token = token;
        $rootScope.setToken(token);
      }
      else{
        window.location = '#/login';
      }
    }
  }
  //regionsCtrl
  if($state.$current.self.name == "regions" ){
    if($rootScope.token == null){
     var token  = window.localStorage['token'];
      if(typeof token != "undefined" && token != ""){
        $rootScope.token = token;
         $rootScope.setToken(token);
      }
      else{
        window.location = '#/login';
      }
    }
    $scope.click = function ($event){
      if($event.target.id != "btn-click"){
        $scope.flagShowMenu = false;
      }
    }
    $scope.init = function () {
      $scope.loading();
      Region.all(function(err, data){
        if(err){
          if(err.status == 401){
            // $scope.logout();
          }
          else{          }
          $cordovaToast.showLongBottom(err.errors).then(function(success) {}, function (error) {});
          $scope.hiding();
          $scope.logout();
        }
        else{
          $scope.hiding();
          $scope.$parent.gListRegion = data.regions;
        }
      });
    }
     $scope.init();
  }
  //newregionCtrl
  if($state.$current.self.name == "newRegion" ){
    if($rootScope.token == null){
      var token  = window.localStorage['token'];
      if(typeof token != "undefined" && token != ""){
        $rootScope.token = token;
         $rootScope.setToken(token);
      }
      else{window.location = '#/login';}
    }
    $scope.NewRegion ={};
    $scope.AddRegion = function(){
      if($scope.NewRegion.name.length==0){
        $rootScope.showError("Please input Location name");
        return;
      }
      else{
        Region.create($scope.NewRegion,function(err, data){
          if(err){
            if(err.status == 401){
                $scope.logout();
              }
              else{
            $cordovaToast.showLongBottom(err.errors).then(function(success) {}, function (error) {});
          }
          }
          else {
            data.region.nodes = 0;
            $scope.$parent.gListRegion.push(data.region);
            window.location = '#/regions';
          }
        });
      }
    }
  }
  //gardenCtrl
  if($state.$current.self.name == "region" ){
    $scope.stateregionId = $stateParams.regionId;
    if($rootScope.token == null){
      var token  = window.localStorage['token'];
      if(typeof token != "undefined" && token != ""){
        $rootScope.token = token;
         $rootScope.setToken(token);
      }
      else{
        window.location = '#/login';
      }
    }
    if(typeof $stateParams.regionId != 'undefined'){
      $scope.loading();
        Region.getById($stateParams.regionId, function(err,data){
          if(err){
            $scope.hiding();
            if(err.status == 401){
              $scope.logout();
            }
            else{
              $cordovaToast.showLongBottom(err.errors).then(function(success) {}, function (error) {});
            }
          } 
          else
            {
              $scope.hiding();
              $scope.$parent.gCurrentRegion = data.region;
              $scope.$parent.gListDevice = data.nodes;
              var lstuuid = [];
              for (var i = $scope.$parent.gListDevice.length - 1; i >= 0; i--) {
                lstuuid.push($scope.$parent.gListDevice[i].uuid);
              };
             if($rootScope.token !=null){
              socket = io.connect("http://api.ubisen.com:80?token="+$rootScope.token);
             }
             // if($scope.$parent.socket!=0){
              socket.emit('subcribe', lstuuid, function(data)
                {
                  if(data.status==201)
                  {
                    // alert('subcribe')
                    // $scope.$parent.addMessage(Math.floor((Math.random()*6)+1),'SmartGrow','subcribe complete ');
                    // $rootScope.showError('subcribe thanh cong');
                  }
                  else if (data.status==400)
                  {
                    // $scope.$parent.addMessage(Math.floor((Math.random()*6)+1),'SmartGrow','subcribe fail ');
                  }
                });
                // $scope.addEmit(lstuuid);
             // }
              
              for (var i = $scope.$parent.gListDevice.length - 1; i >= 0; i--) {
                var dayString  =  "";
                var dayItem = new Date($scope.$parent.gListDevice[i].createdAt);
                var totalDays = (new Date()).getDaysBetween(dayItem);
                if(totalDays ==0 ){
                  dayString+="Today";
                }
                else{
                  dayString += Math.abs(totalDays) +" days ago";
                }
                dayString+= " at "+ dayItem.getHours() +":"+dayItem.getMinutes();
                $scope.$parent.gListDevice[i].labelString = dayString;
                if(Math.abs(totalDays)>=2){
                  $scope.$parent.gListDevice[i].labelString = "<span style='color:red'>" + $scope.$parent.gListDevice[i].labelString +"</span>";  
                }
              };            
            }
        });
    }
    $scope.click = function ($event){
      if($event.target.id != "btn-click"){
        $scope.flagShowMenu = false;
      }
    }
    $scope.flagShowMenu = false;
  }
  //scancodeCtrl
  if($state.$current.self.name == "newNode" ){
    if($rootScope.token == null){
      var token  = window.localStorage['token'];
      if(typeof token != "undefined" && token != ""){
        $rootScope.token = token;
         $rootScope.setToken(token);
      }
      else{
        window.location = '#/login';
      }
    }
    $scope.stateregionId = $stateParams.regionId;
    $scope.newNode ={regionId:$stateParams.regionId};

    $scope.scanBarcode = function() {
        $cordovaBarcodeScanner
        .scan()
        .then(function(imageData) {
          var dt = JSON.parse(imageData.text);
          $scope.newNode.label = dt.label;
        }, function(err) {
          $cordovaToast.showLongBottom(err.errors).then(function(success) {
            }, function (error) {
            });
        });
      };

    $scope.saveNode = function(){
      if($scope.newNode.name.length > 0 &&  $scope.newNode.label.length > 0 && $scope.newNode.regionId.length > 0 ){
        Device.create($scope.newNode, function(err, data){
          if(err){
            if(err.status == 401){
              $scope.logout();
            }
            else{
              $cordovaToast.showLongBottom(err.errors).then(function(success) {
                }, function (error) {
                });
                $rootScope.showError(err.errors);
              }
          }
          else{
            var dayString  =  "";
                var dayItem = new Date(data.device.createdAt);
                var totalDays = (new Date()).getDaysBetween(dayItem);
                if(totalDays ==0 ){
                  dayString+="Today";
                }
                else{
                  dayString += Math.abs(totalDays) +" days ago";
                }
                dayString+= " at "+ dayItem.getHours() +":"+dayItem.getMinutes();
                data.device.labelString = dayString;
                if(Math.abs(totalDays)>=2){
                  data.device.labelString = "<span style='color:red'>" + data.device.labelString +"</span>";  
                }
                $scope.$parent.gListDevice.push(data.device);
                $.map($scope.$parent.gListRegion,function(regionItem){
                  if(regionItem.id == $scope.$parent.gCurrentRegion.id){
                    regionItem.nodes++;
                  }
                })
            window.location = '#/regions/'+$scope.$parent.gCurrentRegion.id;
          }
        });
      }
    }
  }
  //

  if($state.$current.self.name == "bridge" ){
    var flatRefesh = false;
    var flagMessage = false;
    if($rootScope.token == null){
      var token  = window.localStorage['token'];
      if(typeof token != "undefined" && token != ""){
        $rootScope.token = token;
         $rootScope.setToken(token);
         flatRefesh = true;
      }
      else{
        window.location = '#/login';
      }
    }
     $scope.position = {
      min: 0,
      max: 0,
      ceil: 100,
      floor: 0
    };
    $scope.translate = function(value)
    {
      if(timeoutId !== null) {
          return;
      }
      timeoutId = $timeout( function() {
          $timeout.cancel(timeoutId);
          timeoutId = null;
          // if($scope.Slider.position[0] ==-1&& $scope.Slider.position[1]==-1){
            if($scope.position.max ==0&& $scope.position.min==0){

          }
          else{
            $scope.$apply();
            $scope.edit(1);  
            }
            
      }, 2000); 
        return value;
    }
    $scope.stateregionId = $stateParams.regionId;
    $scope.NodeId = $stateParams.id;
    $scope.flagChart = 1;//1.Day,2.Week
    // $scope.Slider = {};
    // $scope.Slider.position = [-1,-1];
    $scope.toDay = new Date();
    $scope.LDay = new Date();
    $scope.FDay = new Date();
    $scope.FDay.setDate($scope.FDay.getDate() - 6);
    $scope.loading();
    Device.getById($scope.NodeId,function(err, data){
      if(err){
        $scope.hiding();
        if(err.status == 401){
          $scope.logout();
        }
        else{
          $cordovaToast.showLongBottom(err.errors).then(function(success) {}, function (error) {});
        }
        }
        else{
          $scope.hiding();
          $scope.$parent.gCurrentDevice = data.device;
          $scope.$parent.gListSensor = data.sensors;
          if(data.sensors.length>0){
            // $scope.Slider.position = [parseInt(JSON.parse(data.sensors[0].configAlert)[0].min),parseInt(JSON.parse(data.sensors[0].configAlert)[0].max)];
            var trigger = JSON.parse(data.sensors[0].triggers);
            if(trigger.length== 0){
              $scope.position.max=0;
              $scope.position.min=0;
            }
            else{
                $.map(trigger,function(ele,indx){
                    if(ele.priority == 1){
                      $scope.position.min = ele.value;        
                    }
                    if(ele.priority == 2){
                      $scope.position.max = ele.value;        
                    }
                });
            }
            // $scope.position.max = parseInt(JSON.parse(data.sensors[0].configAlert)[0].max);
            //   $scope.position.min = parseInt(JSON.parse(data.sensors[0].configAlert)[0].min);
             $scope.AlertValue = data.sensors[0].alertActive;
          }
          if($scope.$parent.gListSensor.length>0)
          $scope.getDayChart($scope.toDay, $scope.toDay);
        }
    });
     $scope.prevouSlide = function() {
        if($scope.flagChart == 1){
          $scope.toDay.setDate($scope.toDay.getDate()-1);
         $scope.getDayChart($scope.toDay, $scope.toDay);
        }
        else
        {
          $scope.LDay.setDate($scope.LDay.getDate()-7);
          $scope.FDay.setDate($scope.FDay.getDate() - 7);
          $scope.getDayChart($scope.FDay, $scope.LDay);
        }
    };
     $scope.nextSlide = function() { 
       if($scope.flagChart == 1){   
          $scope.toDay.setDate($scope.toDay.getDate()+1);
          $scope.getDayChart($scope.toDay,$scope.toDay);
        }
        else{
          $scope.LDay.setDate($scope.LDay.getDate()+7);
          $scope.FDay.setDate($scope.FDay.getDate() + 7);
          $scope.getDayChart($scope.FDay, $scope.LDay);
        }
    };
    $scope.getDayChart = function(timeF,timeL){
      $scope.chartConfig.loading = true;
      var tempF = new Date(timeF.getFullYear(),timeF.getMonth(),timeF.getDate(),0,0,0,0);
      var tempL = new Date(timeL.getFullYear(),timeL.getMonth(),timeL.getDate(),23,59,59,0);
      var interval = 0;
      if(tempF.getDate() == tempL.getDate()){
        interval = 1800;
      }
      else{
        interval = 3600;
      }
      if($scope.$parent.gListSensor.length>0)
      Device.getByTime({uuid:$scope.$parent.gCurrentDevice.uuid,name:$scope.$parent.gListSensor[0].name,start:Date.UTC(tempF.getFullYear(),tempF.getMonth(),tempF.getDate(),tempF.getHours(),tempF.getMinutes(),tempF.getSeconds(),0),end:Date.UTC(tempL.getFullYear(),tempL.getMonth(),tempL.getDate(),tempL.getHours(),tempL.getMinutes(),tempL.getSeconds(),0),interval:interval,limit:-1,token:$scope.$parent.gCurrentDevice.token},function(err,data){
        if(err){
          if(err.status == 401){
                $scope.logout();
              }
          else{
            // $rootScope.showError(err.errors);
            $cordovaToast.showLongBottom(err.errors).then(function(success) {
            }, function (error) {
            });
            $scope.chartConfig.loading = false;
          }
        }
        else{
          var dataChart = [];
          for (var i = 0; i <data.data.length; i++) {
            var time = new Date(data.data[i].at);
            dataChart.push([time.getTime(),data.data[i].value]);
          };
          $scope.chartConfig.series = [];
          $scope.chartConfig.series.push({
            showInLegend:false,
              data: dataChart
          });
          $scope.chartConfig.loading = false;
          flagMessage = true;
        }
      });
    }
    socket.on('message', function(data) {
        if(flagMessage == true){
          var d = new Date();
          var minD = new Date(d.getFullYear(),d.getMonth(),d.getDate(),0,0,0,0);
          var maxD = new Date(d.getFullYear(),d.getMonth(),d.getDate(),23,59,59,0);
          if($scope.$parent != null && typeof $scope.$parent.gCurrentDevice !="undefined")
           {
            if($scope.flagChart == 1){
             if($scope.toDay.getDate() == d.getDate() && $scope.toDay.getMonth() == d.getMonth() && $scope.toDay.getFullYear() == d.getFullYear() ){
              if($scope.$parent.gCurrentDevice.uuid == data.message.deviceUuid && $scope.$parent.gListSensor[0].id == data.message.sensorId){
                var requestTime = data.message.at;
                requestTime = new Date(requestTime);
                if( requestTime.getTime() >=minD.getTime() && requestTime.getTime() <= maxD.getTime()){
                  $scope.chartConfig.series[0].data.push([requestTime.getTime(),data.message.value]);
                  $scope.$apply(); 
                }
              }
             }
           }
           else{
            var minFirstDay = new Date($scope.FDay.getFullYear(),$scope.FDay.getMonth(),$scope.FDay.getDate(),0,0,0,0);
            var minLastDay = new Date($scope.LDay.getFullYear(),$scope.LDay.getMonth(),$scope.LDay.getDate(),23,59,59,0);
            if(minFirstDay.getTime() <= d.getTime() && d.getTime() <= minLastDay.getTime() ){
              if($scope.$parent.gCurrentDevice.uuid == data.message.deviceUuid && $scope.$parent.gListSensor[0].id == data.message.sensorId){
                var requestTime = data.message.at;
                requestTime = new Date();
                if( requestTime.getTime() >=minD.getTime() && requestTime.getTime() <= maxD.getTime()){
                  $scope.chartConfig.series[0].data.push([requestTime.getTime(),data.message.value]);
                  $scope.chartConfig.loading = false;
                  $scope.$apply(); 
                }
              }
            }
           }
           }
        }
    });
    $scope.slidestop = function($index) {
      $ionicSlideBoxDelegate.enableSlide(false);
    };
    $scope.changeCurrentSlideHandler = function($index) {
        var sliderBox;
        sliderBox = $ionicSlideBoxDelegate.$getByHandle("slider-intro");
        if (parseInt($index, 10) === sliderBox.slidesCount() - 1) {
          sliderBox.stop();
        }
      };
    $scope.chartConfig = {
          global: {
              useUTC: false
              // timezoneOffset: 7
          },
          options: {
              chart: {
                  type: 'line',
                  zoomType: 'x'
              }
          },
          loading: false,
          xAxis: {
             minorTickWidth:1, // this is by default 0 and is why you don't see ticks
             minorGridLineWidth: 0, 
              minorTickInterval: 7 * 24 * 3600 * 1000,
              minorTickPosition:'outside',
              minorTickLength:1000000,
              type:'datetime',
              // min: Date.UTC(2015, 0, 7),
              // max: Date.UTC(2015, 0, 7),
              labels: {
                  step: 1,
              },
              dateTimeLabelFormats: {
                  month: '%b',
                  year: '%Y'
              }
          },
           yAxis: {
              min: 0,
              title: {
                  text: 'Value'
              }
          },
          series: [{
            showInLegend:false,
            data: [   ]
          }],
          title: {
              text: ''
          },
          loading: false,
          "credits": {
            "enabled": false
          },
          size:{
            width: null,
            height:250
          }
      };
    $scope.chartConfig.size.width = $window.innerWidth -10;
    window.onresize = function(event) {
     $scope.chartConfig.size.width = $window.innerWidth -10;
     $scope.$apply();
    };
    $scope.changeDateTime = function(input){
     $scope.flagChart = input;
     if(input == 1){
        $scope.toDay = new Date();
        $scope.getDayChart($scope.toDay, $scope.toDay);
     }
     else{
      $scope.getDayChart($scope.FDay, $scope.LDay);
     }
    }
    $scope.AlertValue = false;
    $scope.changeValueAlert = function(){
      $scope.AlertValue  = !$scope.AlertValue ;
      $scope.edit(2);
    }

    var timeoutId = null;
    $scope.changeSlider = function(){
      if(timeoutId !== null) {
          return;
      }
      timeoutId = $timeout( function() {
          $timeout.cancel(timeoutId);
          timeoutId = null;
          // if($scope.Slider.position[0] ==-1&& $scope.Slider.position[1]==-1){
            if($scope.position.max ==-1&& $scope.position.min==-1){

          }
          else{
            $scope.$apply();
            $scope.edit(1);  
            }
            
      }, 2000); 
    };
    $scope.edit = function(input){
      if($scope.$parent.gListSensor.length>0)
      {
        if(input ==2 ){
          $scope.$parent.gListSensor[0].alertActive = $scope.AlertValue;
        }
        var tempPosition = {min:angular.copy($scope.position.min),max:angular.copy($scope.position.max)};
        Device.edit({device:$scope.$parent.gCurrentDevice,sensor:$scope.$parent.gListSensor[0],value:tempPosition}, function(err, data){
          if(err){
            if(err.status == 401){
              $scope.logout();
            }
            else{


              // $rootScope.showError(err.errors);
              $cordovaToast.showLongBottom(err.errors).then(function(success) {
            }, function (error) {
            });
            }
          }
          else{
            // console.log("okEdit");
          }
        });
      }
      else{
         $rootScope.showError("No sensor");
        if(input == 1){
          $scope.position.max = 0;
          $scope.position.min = 0;
        }
        else{
          $scope.AlertValue  = false ;
        }
      }
    }
    $scope.Deactivate = function(input){
      $scope.$parent.gCurrentDevice.isActive = input;
      Device.editActive($scope.$parent.gCurrentDevice, function(err, data){
          if(err){
            if(err.status == 401){
              $scope.logout();
            }
            else
            {
              $scope.$parent.gCurrentDevice.isActive = !input;
              // $rootScope.showError(err.errors);
              $cordovaToast.showLongBottom(err.errors).then(function(success) {
              }, function (error) {
              });
            }
          }
          else{
            // console.log("Deactivate okEdit");
          }
        });
    }
    $scope.click = function ($event){
      if($event.target.id != "btn-click"){
        $scope.flagShowMenu = false;
      }
    };
  }
  $scope.logout = function(){
    User.logout(function(err,data){
      if(err){
        // console.log(err);
      }
      else{
        $rootScope.token =null;
        delete $http.defaults.headers.common['token'];
        window.localStorage['token'] = "";
        socket = null;
        $scope.$parent.gListRegion= [];
        $scope.$parent.gListDevice= [];
        $scope.$parent.gListSensor= [];
        window.location = '#/login';
      }
    });
  };
  $scope.$watch('$viewContentLoaded',function(event){
     $templateCache.removeAll();
  });
  if(socket != null){
    socket.on('connect', function() {
        // $rootScope.showError('connect socket complete!');
    });
    // socket.on('message', function(data) {
    //   if($state.$current.self.name == "bridge"){
    //     $rootScope.showError('nhan message');
    //     var d = new Date();
    //     var minD = new Date(d.getFullYear(),d.getMonth(),d.getDate(),0,0,0,0);
    //     var maxD = new Date(d.getFullYear(),d.getMonth(),d.getDate(),23,59,59,0);
    //     if($scope.$parent != null && typeof $scope.$parent.gCurrentDevice !="undefined")
    //      {
    //       if($scope.flagChart == 1){
    //        if($scope.toDay.getDate() == d.getDate() && $scope.toDay.getMonth() == d.getMonth() && $scope.toDay.getFullYear() == d.getFullYear() ){
    //         if($scope.$parent.gCurrentDevice.uuid == data.message.deviceUuid && $scope.$parent.gListSensor[0].id == data.message.sensorId){
    //           var requestTime = data.message.at;
    //           requestTime = new Date(requestTime);
    //           if( requestTime.getTime() >=minD.getTime() && requestTime.getTime() <= maxD.getTime()){
    //             $scope.chartConfig.series[0].data.push([requestTime.getTime(),data.message.value]);
    //             $scope.$apply(); 
    //           }
    //         }
    //        }
    //      }
    //      else{
    //       var minFirstDay = new Date($scope.FDay.getFullYear(),$scope.FDay.getMonth(),$scope.FDay.getDate(),0,0,0,0);
    //       var minLastDay = new Date($scope.LDay.getFullYear(),$scope.LDay.getMonth(),$scope.LDay.getDate(),23,59,59,0);
    //       if(minFirstDay.getTime() <= d.getTime() && d.getTime() <= minLastDay.getTime() ){
    //         if($scope.$parent.gCurrentDevice.uuid == data.message.deviceUuid && $scope.$parent.gListSensor[0].id == data.message.sensorId){
    //           var requestTime = data.message.at;
    //           requestTime = new Date();
    //           if( requestTime.getTime() >=minD.getTime() && requestTime.getTime() <= maxD.getTime()){
    //             $scope.chartConfig.series[0].data.push([requestTime.getTime(),data.message.value]);
    //             $scope.chartConfig.loading = false;
    //             $scope.$apply(); 
    //           }
    //         }
    //       }
    //      }
    //      }
    //   }
      
    // });
  }
  ionic.Platform.ready(function(){
      window.plugin.notification.local.onclick = function (id, state, json) {
          alert("€you clicked id: "+id+" -"+json);
      }
  });
})

;
