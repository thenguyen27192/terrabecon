angular.module('starter.services', [])
.factory('colorpicker', function () {
      function hexFromRGB(r, g, b) {
        var hex = [r.toString(16), g.toString(16), b.toString(16)];
        angular.forEach(hex, function(value, key) {
          if (value.length === 1) hex[key] = "0" + value;
        });
        return hex.join('').toUpperCase();
      }
      return {
        refreshSwatch: function (r, g, b) {
          var color = '#' + hexFromRGB(r, g, b);
          angular.element('#swatch').css('background-color', color);
        }
      };
    })
.factory('uiSliderConfig', function ($log) {
      return {
          start: function (event, ui) { $log.info('Event: Slider start - set with uiSliderConfig', event); },
          stop: function (event, ui) { $log.info('Event: Slider stop - set with uiSliderCOnfig', event); },
      };
    })
.factory('ionPlatform', function( $q ){
    var ready = $q.defer();

    ionic.Platform.ready(function( device ){
        ready.resolve( device );
    });

    return {
        ready: ready.promise
    }
})
.factory('User', function($http,$rootScope) {
  return {
    all: function(callback) {
      $http.get($rootScope.hostName+'/users')
      // $http({
      //        method: 'GET',
      //        url: $rootScope.hostName+'/users',
      //        headers: {
      //          'Content-Type': undefined
      //        }
      //        // data: ''
      //       })
        .success(function (data) {
          return callback(null,data);
        }).error(function (err,status) {
          // console.log(err);
          return callback({errors:err.errors,status:status},null);
        });
    },
    getById: function(id) {
      // $http.get($rootScope.hostName+'/users/'+id)
      $http({
             method: 'GET',
             url: $rootScope.hostName+'/users/'+id,
             headers: {
               'Content-Type': undefined
             }
             // data: ''
            })
        .success(function (data) {
          return data;
        }).error(function (err,status) {
          
        });
    },
    create:function (user,callback) {
      $http.post($rootScope.hostName+'/users/',user)
      // $http({
      //        method: 'POST',
      //        url: $rootScope.hostName+'/users/',
      //        headers: {
      //          'Content-Type': undefined
      //        },
      //        data: user
      //       })
        .success(function (data) {
          return callback(null,data);
        }).error(function (err,status) {
          return callback({errors:err.errors,status:status},null);
        });
    },
    edit:function (user) {
      $http.put($rootScope.hostName+'/users/',user)
      // $http({
      //        method: 'PUT',
      //        url: $rootScope.hostName+'/users/',
      //        headers: {
      //          'Content-Type': undefined
      //        },
      //        data: user
      //       })
        .success(function (data) {
          return data;
        }).error(function (err,status) {
          
        });
    },
    login:function (user,callback) {
      $http.post($rootScope.hostName,user)
      // $http({
      //        method: 'POST',
      //        url: $rootScope.hostName,
      //        headers: {
      //          'Content-Type': 'application/json'
      //        },
      //        data: user
      //       })
      .success(function (data) {
          return callback(null,data);
        }).error(function (err,status) {
          return callback({errors:err.errors,status:status},null);
        });
    },
    logout:function (callback) {
      $http.get($rootScope.hostName+"/logout")
      // $http({
      //        method: 'GET',
      //        url: $rootScope.hostName+"/logout",
      //        headers: {
      //          'Content-Type': undefined
      //        }
      //       })
        .success(function (data) {
          return callback(null,data);
        }).error(function (err,status) {
          return callback({errors:err.errors,status:status},null);
        });
    }
  }
})
.factory('Region', function($http,$rootScope) {

  return {
    all: function(callback) {
      $http.get($rootScope.hostName+'/regions')
        .success(function (data) {
          return callback(null,data);
        }).error(function (err,status) {
          return callback({errors:err.errors,status:status},null);
        });
    },
    getById: function(id,callback) {
      $http.get($rootScope.hostName+'/regions/'+id)
        .success(function (data) {
          return callback(null,data);
        }).error(function (err,status) {
          return callback({errors:err.errors,status:status},null);
        });
    },
    create:function (region,callback) {
      $http.post($rootScope.hostName+'/regions/',region)
        .success(function (data) {
          return callback(null,data);
        }).error(function (err,status) {
          return callback({errors:err.errors,status:status},null);
        });
    },
    edit:function (region, callback) {
      $http.put($rootScope.hostName+'/regions/',region)
        .success(function (data) {
          return callback(null,data);
        }).error(function (err,status) {
          return callback({errors:err.errors,status:status},null);
        });
    }
  }
})
.factory('Device', function($http,$rootScope) {
    var config = {
            headers: { 
                'Cache-Control': 'no-cache',
                'Content-Type': 'application/json'
            }
        };
  return {
    all: function(callback) {
      $http.get($rootScope.hostName+'/devices')
        .success(function (data) {
          return callback(null,data);
        }).error(function (err,status) {
          return callback({errors:err.errors,status:status},null);
        });
    },
    getById: function(id,callback) {
      $http.get($rootScope.hostName+'/devices/'+id)
        .success(function (data) {
          return callback(null,data);
        }).error(function (err,status) {
          return callback({errors:err.errors,status:status},null);
        });
    },
    getByTime: function(data,callback) {
      $http.defaults.headers.common['api-key'] = data.token;
      var string =$rootScope.hostName+'/devices/'+data.uuid+'/'+data.name+'?start='+data.start;
      if(data.end != -1){
        string+='&end='+data.end;
      }
      // if(data.limit != -1){
      //   string+='&limit='+data.limit;
      // }
      // string+='&interval='+data.interval;
      $http.get(string)
        .success(function (data) {
          return callback(null,data);
        }).error(function (err,status) {
          return callback({errors:err.errors,status:status},null);
        });
    },
    create:function (device,callback) {
      $http.post($rootScope.hostName+'/devices/ubisen/',device)
        .success(function (data) {
          return callback(null,data);
        }).error(function (err,status) {
          return callback({errors:err.errors,status:status},null);
        });
    },
    editActive:function (device,callback) {
      $http.put($rootScope.hostName+'/devices/'+device.id,{isActive:device.isActive})
        .success(function (data) {
          return callback(null,data);
        }).error(function (err,status) {
          return callback({errors:err.errors,status:status},null);
        });
    },
    edit:function (data,callback) {
      if(data.sensor.SensorTypeId == null){
        data.sensor.SensorTypeId = 0;
      }
      $http.put($rootScope.hostName+'/devices/'+data.device.uuid+'/resources/'+data.sensor.name,{configAlert:[{ level:0 ,min:data.value.min ,max:data.value.max }],alertActive:data.sensor.alertActive,SensorTypeId:data.sensor.SensorTypeId})
        .success(function (data) {
          return callback(null,data);
        }).error(function (err,status) {
          return callback({errors:err.errors,status:status},null);
        });
    }
    // ,
    // delete:function(id,callback){
    //   $http.delete($rootScope.hostName+'/devices/'+id)
    //     .success(function (data) {
    //       return callback(null,data);
    //     }).error(function (err) {
    //       return callback({errors:err.errors,status:status},null);
    //     });
    // }
  }
})
;
